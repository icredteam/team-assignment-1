﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.ObjectModel;
using Valve.VR.InteractionSystem;
public class Orbitor : MonoBehaviour {

    public float xRotate, yRotate, zRotate, speed, radius;
    private float fastSpeed;
    private float currSpeed;
    private int segments = 75;  // Segments refers to how many points on the line will be drawn
    private LineRenderer lr;    // draws line
    private GameObject sun;

    // Needed for trigger press
    public Valve.VR.SteamVR_ActionSet m_actionSet;
    private Valve.VR.SteamVR_Input_Sources inputSources;
    public Valve.VR.SteamVR_Action_Boolean TriggerClick;

    // initialize trigger functions
    private void Awake() {
        TriggerClick.AddOnStateDownListener(PressD, inputSources);
        TriggerClick.AddOnStateUpListener(PressU, inputSources);
    }

    // sets position of planet and defines information about the line to be drawn
    void Start() {
        m_actionSet.Activate(Valve.VR.SteamVR_Input_Sources.Any, 0, true);
        lr = gameObject.GetComponent<LineRenderer>();
        lr.positionCount = segments + 1;
        lr.material.color = Color.red;
        lr.startWidth = .05f;
        CreatePoints();
        fastSpeed = speed * 3;
        currSpeed = speed;
        sun = GameObject.Find("Sun");
        this.transform.position = new Vector3(radius + sun.transform.position.x, sun.transform.position.y, sun.transform.position.z);
    }

    // moves planet in circle and rotates planet
    void Update() {
        if (Input.GetButtonDown("planet_press")) {
            currSpeed = fastSpeed;
        }
        if (Input.GetButtonUp("planet_press")) {
            currSpeed = speed;
        }
        transform.RotateAround(this.transform.parent.position, new Vector3(0, 1, 0), currSpeed * Time.deltaTime);
        //this.transform.position = new Vector3(radius * Mathf.Cos(currSpeed * Time.time) + sun.transform.position.x, sun.transform.position.y, radius * Mathf.Sin(currSpeed * Time.time) + sun.transform.position.z);
        this.transform.Rotate(new Vector3(xRotate, yRotate, zRotate));

    }

    void OnControllerColliderHit(ControllerColliderHit hit) {

    }

    // this draws out all the points. i refers to which point being draw, and the rest traces out a circle
    // angle needs to be incremented to change the points position on the line
    // look here for more info: https://forum.unity.com/threads/linerenderer-to-create-an-ellipse.74028/

    void CreatePoints() {

        float angle = 15f;

        for (int i = 0; i < (segments + 1); i++) {

            lr.SetPosition(i, new Vector3(radius * Mathf.Cos(Mathf.Deg2Rad * angle), 40, radius * Mathf.Sin(Mathf.Deg2Rad * angle) - 65));
            angle = angle += (360f / segments);
        }
    }

    // trigger press function
    private void PressD(Valve.VR.SteamVR_Action_Boolean fromAction, Valve.VR.SteamVR_Input_Sources fromSource) {
        currSpeed = fastSpeed;
    }

    //trigger released function
    private void PressU(Valve.VR.SteamVR_Action_Boolean fromAction, Valve.VR.SteamVR_Input_Sources fromSource) {
        currSpeed = speed;
    }
}
